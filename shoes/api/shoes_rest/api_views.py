from django.http import JsonResponse
from .models import Shoe
import json
from django.views.decorators.http import require_http_methods
from common.json import ModelEncoder


class ShoeDetailEncoder(ModelEncoder):
    model = Shoe
    properties = [
        "manufacturer",
        "model_name",
        "color",
         "picture_url",
        # "bin"
    ]

class ShoeListEncoder(ModelEncoder):
    model = Shoe
    properties = ["model_name", "color"]

@require_http_methods(["GET", "POST"])
def api_list_shoe(request):
    if request.method == "GET":
        shoes = Shoe.objects.all()
        return JsonResponse({"shoes": shoes}, encoder=ShoeListEncoder)
    else:
        content = json.loads(request.body)
        shoes = Shoe.objects.create(**content)
        return JsonResponse(
            shoes,
            encoder = ShoeDetailEncoder,
            safe= False,
        )

# def api_show_shoe(request, pk):

#     shoe = Shoe.objects.get(id=pk)
#     return JsonResponse(
#         {
#             "manufacturer": shoe.manufacturer,
#             "model_name": shoe.model_name,
#             "color": shoe.color,
#             "picture_url": shoe.picture_url,


#         }
#     )