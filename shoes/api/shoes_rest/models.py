from django.db import models
from django.urls import reverse


# Create your models here.
class BinVO(models.Model):
    closet_name = models.CharField(max_length=100)
    bin_number=models.PositiveSmallIntegerField()
    bin_size=models.PositiveSmallIntegerField()

class Shoe(models.Model):
    manufacturer = models.CharField(max_length=200)
    model_name = models.CharField(max_length=200)
    color = models.CharField(max_length=200)
    picture_url = models.URLField(null=True)
    # bin = models.ForeignKey(
    #     BinVO,
    #     related_name="bin",
    #     on_delete=models.PROTECT,
    

def __str__(self):
    return self.name

def get_api_url(self):
    return reverse("api_list_shoes")
