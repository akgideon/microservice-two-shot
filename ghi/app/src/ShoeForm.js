import React from 'react';

class ShoeForm extends React.Component {
    async componentDidMount() {
        const url = 'http://localhost:8080/api/shoes';

        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            this.setState({shoes: data.shoes});
        }
    }
    render() {
      return (
        <div className="row">
          <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
              <h1>Create a new shoe!</h1>
              <form id="create-shoe-form">
                <div className="form-floating mb-3">
                  <input placeholder="Manufacturer" required type="text" name="manufacturer" id="manufacturer" className="form-control"/>
                  <label htmlFor="name">Manufacturer</label>
                </div>
                <div className="form-floating mb-3">
                  <input placeholder="Model Name" required type="text" name="model_name" id="model_name" className="form-control"/>
                  <label htmlFor="model_name">Model Name</label>
                </div>
                <div className="form-floating mb-3">
                  <input placeholder="City" required type="text" name="city" id="city" className="form-control"/>
                  <label htmlFor="city">City</label>
                </div>
                <div className="mb-3">
                  <select required name="state" id="state" className="form-select">
                    <option value="">Choose a state</option>
                  </select>
                </div>
                <button className="btn btn-primary">Create</button>
              </form>
            </div>
          </div>
        </div>
      );
    }
  }

export default ShoeForm;